# Foreword #

Each year for the past four years, my good friend Mr. Adam Culp has invited me to present a keynote at the best PHP conference in the world, [Sunshine PHP](https://sunshinephp.com). 

- Each year I am surprised. 
- Each year I make the joke that he must be scraping the bottom of the barrel. 
- Each year I agree, secretly gleeful because I dearly love the sound of my own voice.

This book you are about to read was my presentation for Sunshine PHP 2017. Each year Adam gives all the keynote speakers a "theme" for the conference. We do our best to work within it. This year, unlike years past, when I sat down to write, I did not have an idea in mind. I think Adam had given us the theme of "Aspire" but honestly I rarely pay attention to him once he's asked. Working with aspire though, nothing really jumped out. This was early January. Those that know me know that I usually have my talks written and practiced at least a month in advance. We were less than thirty days out and I was just sitting down to write. I was a little bit panicked.

Then one day I was digging through github to find something when I saw a repo with twenty or thirty of these little chicklet badges on it.  When I first saw it, I honestly I thought it was a joke. I thought the maintainer was just having a laugh. Up to that point, I had seen "Build passing" and if it was a big project they might have one fo the version number. I'd never seen this many shoved into a `Readme.md` before. 

This was no joke. On this repo each of these badges was actually for something. I was amazed. I was amused. I started wanting my repos to have a bunch of these little badges. 

The more I thought about it over the course of a day or so, the more I realized why I wanted them on my projects. I wanted them for the same reason that I wanted projects on my resume, for the same reason Girl Scouts want patches on their sashes. I wanted them because they show progress. They show that you are accomplishing things. They are badges of honor.

The next day when I sat down to pretend to write - seriously, all those times I tweet out that I'm writing, you don't really believe them do you - I went back to this train of thought. I realized that I was aspiring to have those badges on my repos...aspire...that sounds familiar. So now you know how my mind works.

This book started out as my notes for the talk. As I work, I flesh them out into a full blown script. This is that script. Near the end, you will notice that I talk about being at a conference. You probably aren't at a conference right now, so mentally change that to talking abut where you are right now. It will save you some confusion and save me having to re-write it just for you.

I hope you enjoy this book as much as I enjoyed writing it. If you have questions or comments, please contact me You can find me on Twitter most days at [&commat;calevans](https://twitter.com/calevans). 

If we are ever at a conference together, don't hesitate to introduce yourself. I promise I don't bite. The introduction that I've found that works the best is "Hi Cal, let me buy you a drink.". But you can stick with the more generic "Hi Cal." if you like. 

Cheers!<br />
=C=

Cal Evans<br />
Jupiter, FL<br />
April 4th, 2017<br />

<div class="pagebreak"></div>