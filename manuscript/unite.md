# Unite #
![](/data/manuscript/images/unite.png)\


Once we learn to start helping people, most of us want to branch out and help as many people as we can. Helping people one at a time is a lot easier than trying to help a lot of people. Once you start to scale your efforts, you realize there are limits to what a single person—any single person —can do. Naturally, we look around and try to find other like-minded people to work with. Soon, there are two people helping instead of just one. As programmers, we are well aware that having two people on a project doesn’t mean it gets done twice as fast, but it does get done faster, even factoring in overhead. 

As you grow your newfound organization, you begin to see the fruits of your efforts. However, like any good project lead, you find yourself doing less and less helping people and doing more organizational tasks. Soon, you have several people working with you to help others, but you’re spending all of your time organizing projects or recruiting new people to the team. Congratulations! You are now a community organizer. You are now helping unite people behind a single cause.

What that cause is, is irrelevant as long as it’s your passion cause. Some people organize others to travel to Africa and drill wells so that communities there can have clean water. Others organize doctors to travel around the world and provide health care in regions of the world where there is little. We tend to lift these organizations up and place them on a pedestal, point to them, and lather them with praise for their efforts. 

To me though a layperson working to organize their local community is every bit as laudable as these large multi-national efforts.

Most of us in the room know Mr. Ed Finkler. If you don’t know Ed, take the time to Google him and see what he’s been up to. I’ve known Ed for more than ten years. I’ve watched him grow into the man he is today, and I am proud to call Ed my friend. 

Over the past five years, Ed has started talking about a problem he suffers from, mental illness. He travels around to camps and conferences around the U.S. speaking anywhere he can. A few years ago, Ed started doing yearly Indiegogo campaigns to help cover the travel costs that conferences can’t cover. At that point, Ed discovered that he was now both a speaker AND a fund raiser, and he rose to the occasion. 

Then, Ed discovered if he wanted companies to donate, he needed to be an official nonprofit entity in the US. I am absolutely positive that Ed could have tackled this problem on his own and gotten the paperwork filed. However, he didn’t have to. Another community member stepped up, Joe Ferguson. Joe had experience in filing for nonprofit status for other organizations, so he helped Ed file for his. Now Joe works with Ed to maintain the organization.

Ed still speaks whenever he can, but now he also helps support other speakers who speak on mental illness in tech as well.  He has a line of “OSMI” (Open Sourcing Mental Illness) products like hoodies and T-shirts that all go to fund his efforts. 

Ed is uniting people around the concept of talking about mental illness in the workplace. He is not only making people aware of the issue; he is uniting others under the OSMI banner to enlist their help in making others aware. Ed doesn’t have the budget the Red Cross has, but that doesn’t mean his work is any less important. In his community, he is having an impact!

Ed isn’t the only one uniting people in our little corner of the world. Do you have a local PHP User Group? If so, you have someone uniting people in your local area. 

My friend, Mr. Jeremy Kendall, was a most unlikely organizer. I first saw Jeremy’s name when I was working at php[architect]. We used to hold webinars to help sell tickets to php[tek]. Jeremy attended one of those webinars. As was our custom, at the end of the webinar, we gave away a free ticket to [tek]. This particular ticket went to Jeremy. 

Later that year, I met Jeremy when he used his free ticket and attended the conference. He was a nice guy, and we hit it off. At that particular conference, we had had a speaker cancel at the last minute, so we did what every conference organizer does in that situation, we threw together a panel discussion. We gathered all the PHP User Group leaders we could find at the conference and announced a panel discussion on starting a user group. 

You can probably tell where this is going, Jeremy was in attendance, caught the user group bug and when he went back home to Memphis, Tennessee he started their group. Jeremy moved on from the group; he moved to Nashville for a while, and in his absence, others stood up and took over. However, the group—which is thriving to this day—would not exist if Jeremy had not caught the bug. Jeremy helped unite the Memphis PHP community around the idea of getting together to learn. His legacy lives on in a thriving group.

For me, this badge is yellow. Some might find that odd as a lot of people look to me as an organizer of the PHP community. I am here to tell you I am not. As much as I inwardly revel at being called “The Godfather of the PHP community” or “The Icon of the PHP community” I do not feel like I have earned it. The community existed long before me. Others built it. At best, I am an amplifier for the community, a cheerleader for those doing the work of uniting the community. 
I make a lot of noise about the community, but people like Jeremy Kendall, Chris Cornutt, Beth Tucker Long, Ben Ramsey, and our host here at the conference, Mr. Adam Culp have all done way more than I have to unite our community.  These people have earned the bright green “Unite” badge. Compared to their efforts, mine don’t stack up. So I’ll keep my Unite badge at yellow for now. Next year, I hope to tell you that I’ve turned it green as I am now the organizer of my own PHP User Group, the Palm Beach Gardens PHP Users Group. Let’s see how this adventure plays out. 
<div class="pagebreak" /></div>