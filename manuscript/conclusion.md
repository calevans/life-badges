# Conclusion #

That’s it; those are the badges I want on my life’s repo. You’ll notice I left that last one gray. I can’t judge whether I have inspired you, or anyone for that matter. It is not a metric I have insight into. It would be wonderful if there were a tool like PHPMD that I could run and get stats on my life’s repo like, “How many people have I inspired?” Sadly, it doesn’t exist. 

Some of the others yes, I have them and I’m doing ok. Others, I have them but need to work on, but I’ll eventually get there. When I look at the badges I have and see that not all of them are bright green, I don’t get depressed; I remember the quote from the great English poet Robert Browning:

“Ah, but a man's reach should exceed his grasp, Or what's a heaven for?”

You are about to spend two wonderful days talking PHP. Take some time in the next two days, find a quiet spot—yes, there are some in this hotel—and reflect for a bit. What Life Badges do you currently have on your life’s repo? Which ones do you want to add? Yours don’t have to be the same as mine. Like badges on code repos, there are an endless supply of badges. Pick the ones that are important to you, focus on them, turn them green.

Thank you.
<div class="pagebreak" /></div>