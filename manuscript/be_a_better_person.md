# Be a Better Person #
![](/data/manuscript/images/better_person.png)\



There is an old adage in psychology, you can’t love others until you learn to love yourself. There is a reason this advice has stuck around all these years; it is true, and applicable to all of us. For you to get the more advanced Life Badges, you have to work on yourself first, so that you can begin to help others later. 

This badge is “Be a Better Person.” This is a fun one because it is wholly up to you. 

- Only you can decide what “Be a Better Person” means for you.
- Only you can measure your progress.

You’ll notice I gave myself bright green on this Life Badge. I gave myself green not because I consider myself a better person. My wife, my kids, heck, even my dog will tell you that I’ve got a long way to go on this front. I game myself green because I can look back on my 36-year professional career and see that I have come a long way. 

When I got into programming computers, I used to say, “I program computers because I don’t like dealing with people.” The sad thing is, it was true. I was then, and still am to this day, an introvert. When I code, I code with headphones on. When I play games on my XBox, I don’t pay attention to who else is playing the game. 

My son and I both play Battlefield I. We both have XBoxes, so we are never in the same room when we play. My inattention to others is so bad, one day between Christmas and New Years as I finished up a game, my son came downstairs and started talking to me about it, recapping it, and discussing where the enemy went wrong, etc. He and I had played a 30-minute game, not only on the same team, but in the same squad of five players, and I hadn’t even noticed!  

Given that starting point, and knowing that I am now a community leader—ok, community cheerleader—you can see that I have come a long, LONG way. I have learned that one of the best parts of being a programmer is when you help someone solve a problem that you’ve already solved. It’s not a moment of gloating, for me, it’s a moment of pure joy. 

I have slowly come to the realization that I really enjoy helping people. My joy from helping people has overcome my introvert tendencies, mostly. I’ve still got a lot of work to do.  

Look back down your career path. Are you still the same person you were when you started? If you started today, maybe the answer is yes. However, as adults, most of us want to be a better person, and whether we do it consciously or subconsciously, we constantly work towards the goal of “better person,” whatever that may mean for us.

This isn’t a badge like, say, a Unit Test Coverage badge where you deal in an absolute number. A Unit Test Coverage badge gives you a percentage of coverage. You are either at 100% and have achieved your goal, or you are not, and you still have work to do. The “Be a Better Person” badge will never have a percentage of completeness until you move on to that great open office in the sky. At that point, yes, you have become the best person you will ever be. We’ll turn your badge bright green before we spray paint it onto your headstone. Until then, you are still a work in progress.

What can you do to become a better person? That is 100% up to you. Only you truly know where you are now and how you define “Better Person.” This is not a badge someone else can color for you; you have to figure it out for yourself. 

The other side of the coin is, nobody else gets to define it for you. If anyone else tells you what you need to do to become a better person, smile and walk away without responding. It’s not worth your time to argue with them. They don’t know; they can’t know because this is 100% internal. 

To progress in your “Better Person” badge, you have to sit down and figure out exactly what the term means to you, and then work towards it. You do this daily. You may not see a lot of progress, but if you work towards it every day, every week, every month, then slowly but surely, you can turn your badge from gray (unknown) to red, then yellow, then green. 

Only you can make progress. Only you can accurately measure that progress.

They say the best time to plant a tree is 20 years ago, the second best time is today. The same goes for earning your “Better Person” badge. The best time to start was years ago, the second best time to start is today.
<div class="pagebreak" /></div>