# Help Someone #
![](/data/manuscript/images/help_someone.png)\



Up to this point, all the Life Badges I aspire to have on my life’s project have focused on me. 

- Make **ME** a better person.
- Make **ME** humble.
- Make **ME** a better developer. 

As with most things in my life, it’s been all about “The Cal.” 

Let’s change that now! As my life’s project evolves, and I start to turn some of these badges green, I need to turn my attention towards others. It just seems kind of pointless to make my life project better if I can’t do the same for others at the same time. 

The first outward facing Life Badge is to help someone, and it’s important to me. It’s important because I wouldn’t be where I am if someone hadn’t reached their hand out to me when I needed it and helped me along the way. 

In 2005, I was unemployed because the startup I was working at had just shut down. Through a series of what can only be described as miracles, I came to the attention of the Chief Marketing Officer of Zend, Mr. Mark de Visser. Mark hired me to work on DevZone. Not to post things, but to write the code. 

Then, the person I was working for got sick and left the company, and I was promoted to “Editor-in-Chief” of DevZone. This came with the auxiliary title of “Community Guy.” We didn’t have the term Developer Advocate back then. That is how I started down the road to being a PHP Community cheerleader. It has been a road with some twists and turns, and even a detour or two. But for the past 12 years, my career focus has been on educating PHP developers—ll because someone helped me.

When I help others, part of it is because I truly enjoy helping others. Part of it, though, is that I am still trying to pay that original debt back. Mark has never once mentioned it to me. He has never indicated he wants payback or that I owe him. Still, I know. I know I owe a debt and that debt has to be paid forward.

So I actively look for ways to help people. If you look around, it’s not hard to find opportunities. Many times I work behind the scenes as I’m really not doing it for recognition. To that end, I won’t regale you with all the wonderful things I’ve done for people over the years. All you have to do is look at the badge above and see it’s not bright green, to know that I don’t always help when I can, or that sometimes when I try, I fail. 

Thankfully, the badge I want isn’t “HELP EVERYONE,” it’s “HELP SOMEONE.” You can’t help everyone; down that road lies disaster. You can dedicate your entire life to helping people and never be able to help a significant percentage of those needing help. Along the way, you’ll start to get so depressed about the big picture that you will quickly burn-out. 

As bad as it sounds, you have to ignore that a lot of people could use your help, and focus on finding one person that can use your help, and helping them. Then, if you want, find someone else and repeat the process. 

Helping someone can be as simple as being an ear for them to chew on for a while. Some people just need to vent to someone they trust. I do that for a lot of people—no, I’m not going to tell you who—that would defeat the purpose. 

Other times, it is more tangible and requires more of your time and effort, like mentoring. Mentoring someone takes a lot of energy. I am in awe of people like Liz Smith and Chris Hartjes who always seem to have two or three people under their wings at any given time. They take their time and invest it in helping others. I can usually only do that for one person at a time. 

Since I enjoy writing, a couple of years ago I put a form up on my blog where people can submit their talk abstracts, and I’ll review them and comment on them. I’ve reviewed over a hundred abstracts by now, and while I don’t get a ton of them, I get enough to keep me busy. It always makes me smile when I am reading a conference’s list of talks, and I recognize an abstract that I helped with. 

Sometimes, though, it takes more than just effort. Sometimes it takes money. I’ve never been a rich person. Growing up, my family was just on the edge of middle class. But I was taught from an early age that when someone needs your help, you give what you can. 

I love the PHP community because I have seen it rally around its own to help in a time of need. Sometimes those have been public campaigns. Other times there have been campaigns that you probably don’t know about and never will. Campaigns that made the difference of “will my family eat tonight” to the recipients. This community has more heart and goodwill than any IT focused community I have ever been a part of. Together, we have made a difference in many people’s lives.

Regardless of how you personally earn your “Help Someone” Life Badge, I urge you to earn it. Start looking today. Look for someone who needs some help. Someone struggling with a technical issue that you can help solve, someone who is wandering around a conference by themselves and really just wants someone or a group to hang with.  You can earn this life badge by simply walking up to someone standing alone at a conference after party and saying, “Hi! My name is...”
<div class="pagebreak" /></div>