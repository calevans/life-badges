# Introduction #

It all started with Continuous Integration. Remember the first time you saw a readme with a build passing badge? We all thought it was cool. Even when it was red and not passing, it was cool. But it wasn’t enough; it’s never enough. So we started a badge for unit testing. It tells you not only that there are tests, but how much of the project is actually covered. That was cool. It was enough, right? Sadly, no. We were only just getting started.

If one badge was good and two was better, then obviously the more, the merrier. Soon the README files of projects were starting to look like a military ribbon bar. We’ve got badges for:

- Your test coverage percentage
- Your Scrutinizer score
- Your Code Climate score
- Your current Packagist release
- Continuous Integration with multiple services
- Number of downloads for the project
- Number of stars for the project (because looking at the top of the page is obviously too hard)
- Your license
- Whether or not your dependencies are up to date
- How many packages depend on yours
- Whether or not the project was tested with HHVM
- Your code consistency
- The stable branch
- Your release branch
- Your unstable release branch
- Your SensioLabs Insight score
- The author’s Twitter handle
- Completeness of documentation
- A reminder to document (separate badge)
- Your Gitter channel
- Your IRC channel

Being the flaming jackass I am, I, of course, created my own badge, the “=C= Approved” badge. Why? Why not!

![](/data/manuscript/images/cal_approved.png)\

If your project doesn’t have at least one badge on it, it’s hard for developers to take you seriously. Forget making sure your project has a license, unit tests, or documentation; make sure you have the right combination of badges!

Badges are the cool thing. They work because they visually communicate information to developers quickly. Some of that information may be useless, but it is easily communicated. 

I want badges for my life! I want my personal project to quickly communicate information    to anyone who looks at me. I want my Life Badges! So, I came up with a list of the badges I want on my life’s project.  Here are the ten badges I want on my life’s projects. 


These are the badges for the goals I aspire to. 

- Learn
- Be a better person
- Humility
- Be a better developer
- Create something
- Help Someone
- Unite
- Greatness
- Leadership
- Inspire

Let's look at these individually.

<div class="pagebreak" /></div>