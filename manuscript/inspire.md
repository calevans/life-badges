# Inspire #
![](/data/manuscript/images/inspire.png)\



The last Life Badge I want on my life’s repo is the “Inspire” badge. If you’ve been following the progression then this, to me, is the logical conclusion. First, I help myself. Next, I help others. Then, I lead others help others. Finally, I inspire others to lead others. If I do it right—and so far I’ve not —I honestly believe I can create a perpetual motion machine of service in the PHP community.

This Life Badge could have easily been “cheerleading” because that is really what I think I do. I am more cheerleader than leader in the PHP community. The job of those of us who are cheerleaders, is to get and keep the community excited. To constantly point out the good things that are happening. Whether in public like this or in private one-on-one sessions, my job is to help you see the good. Sometimes, yes, that involves a halogen beam flashlight and a pick-axe so we can find the good, but the good is always there. 

Whether we are helping someone by simply allowing them to vent because of problems they are experiencing, or helping them find a new job, the job of those of us who are cheerleaders is to help. Any time I personally help someone in the community, and they take the time to thank me, I always follow up with the same thing. “You are welcome; I am always happy to help.” Honestly, I am, and it seems to surprise some people. If it is within my power to help you in your journey, why would I not do it? 

What I don’t say, what I need to start saying, what I have said from this very stage several times, is that if I’ve helped you, I don’t expect you to pay me back, I expect you to pay it forward. 

PHP exists because someone paid it forward. In an interview on SitePoint,  Rasmus talks about the fact that when he started PHP, there was no such thing as open source software. 

When you don’t have the money to buy SCO Unix and you can download something that works and even find people who can help you get it up and running, how can you beat that? 

Yes, PHP exists because Rasmus had an itch and he scratched it. PHP is great, though, because he then shared what he had done with others. He didn’t take the prevailing attitude of the day, “Yes, I can help you count the hits on your webpage, have you got a credit card?” No, he put his work out there for others to not only use for free but to add to. He didn’t expect anything in return.

Rasmus inspired us because he gave of his time and talents. A long line of people follow him in adding to PHP. None of them asked for anything in return. Many of them—like Rasmus—were simply scratching their own itch and sharing the resulting code. In short, all of us who use PHP for a living owe a debt. 

Like the vast number of people who use PHP for profit, you can simply choose to ignore that debt; no one is going to revoke your PHP license if you do. A small percentage, though, understand that they owe the debt. This small percentage of developers that use PHP  acknowledge the debt that they owe. Some of those—but sadly, not all—even attempt to repay it. 

I’m not about to pass the hat for Rasmus. When I say repay the debt, I don’t mean give someone money. I don’t mean buy someone’s T-shirts, or even buy my books; that’s not how you repay this debt. You repay this debt by helping the next person in line. You repay it by sharing what you have learned. 

You repay it by blogging about the new trick you learned, the latest library you have worked with, about the conference you just attended and how awesome the opening keynote was. You repay the debt by offering to speak at your local user group. If you aren’t comfortable on stage, you repay it by volunteering to set up before meetings or tear down after meetings. You repay the debt by helping organize your local User Group. 

Most importantly, you repay the debt by simply answering a question. Whether it is in a Slack channel, IRC, Facebook, Twitter, or your communication medium of choice. When someone has a question, and you know the answer, speak up. Help that person, because someone helped you.

I can’t force you to repay the debt. Heck, I can’t even force you to acknowledge the debt. If you don’t feel that you owe it, well, you can keep on using PHP. Many developers, however, do eventually come to realize that it exists. Slowly, whether consciously or subconsciously, they begin to repay it. They become part of the perpetual motion machine of support I am helping to build. They become active in the PHP community.
<div class="pagebreak" /></div>