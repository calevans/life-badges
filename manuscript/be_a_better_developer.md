# Be a Better Developer #
![](/data/manuscript/images/better_developer.png)\



Are you the best developer you can ever be? If the answer to that question were yes, you wouldn’t be here learning. We can all be better developers. The questions is really—and I say this without sarcasm—do we all really want to be? A lot of us work in this little bubble called the open source community. We expect developers to have a day job, be building a company as their side hustle, and of course, make significant contributions to an open source project as part of their ‘giving back’ initiative. All the while building a life away from the computer screen. To keep up with this rat race, we have to constantly learn new things, keeping up with the shiny, so that when we do contribute, we aren’t using last week’s tools or techniques. 

It is not hard to see why some developers don’t seem to be enamored to be on the open source hamster wheel. Yes, you are constantly honing your skills, but sometimes it comes at too high a price. 

Being a better developer means more than getting the latest certification or adding another repo to your GitHub presence. Being a better developer means learning to be a well-rounded developer. Sometimes that means saying no to things that will improve your skills in the short-term but will have long-term negative effects.

Being a better developer is a balancing act; you balance your desires. Desire for a good career, desire for a life outside of work, desire for companionship, etc. Like every craft or art, developers have to decide how much of their time they want to invest, and how good they want to become. 

Some developers want to be the best. Like a talented musician or a gifted athlete, they are willing to make the personal sacrifices to become great. It’s their passion. Being the best is what drives them forward. There is absolutely nothing wrong with this. These individuals push us forward; they are the ones that push our industry forward. 

In recent months, we’ve started to see a backlash against these driven people. We are being told they’re the reason the rest of us are unhappy, because we can’t keep pace with the best at the level we are willing to invest. This is wrong-headed thinking. Talented individuals should never be held back because the rest of us can’t keep up. They should be allowed to run ahead of the pack. They should go as fast and as far as they want without judgment or condemnation. If they succeed wildly, we should applaud them. We should lift them up as examples to others. They made the sacrifices, and in almost all cases, we are all better for it.

What about the rest of us? What about those of us who aren’t quite as talented developers as the best? Those of us that have other commitments in life or other desires? What about those of us for which programming is just a job, not a lifestyle choice? Well, we’ve made our choices. I know I will never be the best PHP programmer out there. I am arrogant enough to believe that if I committed myself to it and really dug in, I probably could be the best, or at least in the top ten. 

However, for me, I am good enough. I have struck a balance between my drive to be the best programmer and my other desires. I have a wife, the lovely and talented Kathy. I have two wonderful adult children whom I don’t get to spend enough time with. I am a Christian and enjoy spending time in church and studying the Bible. I am a scuba diver. I am a gun enthusiast; I like going to the pistol range. I enjoy writing articles and books. I have to balance all of that with my passion for developing software, and it is a balancing act. 

I’ve come to grips with my limitations as a developer and not paying the price to overcome them. There are people that are far better than me at writing software, and that doesn’t take any of the joy out of it for me. Much to the contrary, finding my balance between all of my desires has actually led to a much more peaceful life for me. 

I won’t pretend it is easy to find the right balance. I was well into my 40s before I even figured out that I needed one. Then, it took time to figure out how to do it. As I was studying how others did it, reading books, reading blogs, trying desperately to figure out how to have it all, I discovered the truth. What has worked for others, won’t work for you. There is no formula to follow to achieve balance. There is no “10 Steps to a Balanced Life.” You have to figure out what balance means to you.

If you decide that being the best is your balance, I applaud you. I encourage you. I look forward to the great things you will achieve.

If you decide your other desires are equally important to your desire to program, wonderful. You are absolutely no better or no worse than those who have decided to sacrifice for their craft. No one should tell you how wrong you are, just as you have no right to criticize those who run ahead of the pack. 

Once you find your balance, and once you begin to apply that balance to your life, you will find that you become a better developer, because you are now a happy developer. 
<div class="pagebreak" /></div>