# Create Something #
![](/data/manuscript/images/create_something.png)\


I’m gonna start out this one by saying, yes, I gave myself bright green on this one. Not because I have created everything there is to create, and I’m done; this is a scale of effort, and I put a lot of effort into creating things. 

Creating things isn’t new to me. Monetization, minimum viable product, bootstrapping, product-market fit, these are all relatively new to me, but I’ve been creating as long as I could hold a pencil. For the longest time, my creative outlet was writing. I couldn’t draw, and people told me I couldn’t sing. Honestly, I think they might have been lying because I could belt out Sesame Street with the best of them. I could write, though, and so I did.

Fast forward to the early ‘80s, and I discovered computer programming. For my 18th  birthday, my parents bought me a Commodore 64, and from that point on, I was hooked. Since then, I have logged countless hours behind a keyboard both professionally and for fun. 

When I was 21, I formed my first software company, Tranquility Base Software. Two friends and I friends were going to finish and publish a game one of us had written that was essentially a knock-off of the old arcade machine ‘Burger Time.” We didn’t get very far, but boy did we have fun. We eventually dissolved Tranquility Base Software and went our separate ways, but there for a while, I was part of my first software development team. 
 
Along the way, I’ve created somewhere around 25 products. Now, given that I’ve only been programming for 34ish years, that’s a pretty good track record. Not all of these products saw the light of day, thankfully. But they count because I had an idea and executed it.

Building things has always been in my blood, and since I lack any other talent, programming has always been my tool of choice for expressing my creativity.

The thing is, I have learned lessons in 34 years of side hustles that I would never have learned if I had just stuck to my day job. For example, I learned early on—back in my 4 bit C=64 days, that while I dearly love programming, that love does not extend to machine language programming. I did it once for a project, found out I am horrible at it, and never looked back. My side projects have always been different than the things I work on for my day job. That’s what makes them interesting. That’s why I learn new things. 

It always pains me to see what we’ve come to call “All your bases are belong to us” employment contracts. You know the ones; the company claims rights to anything you may invent whether or not you invent it as part of work. You may be under one of these contracts now. Here’s another lesson I’ve learned because of my side projects—everything is negotiable. If your side projects are important to you, when you move to your next job, negotiate that clause down. The language I always suggest is,“anything the company causes to be written.” That way if I am building purple widgets and I write a script that automates building purple widgets, the company owns that. If on the other hand, I create “Uber, but for Penguins,” they’ve got no claim on that. You would be surprised how many companies will negotiate this clause down to something reasonable. 

Creating new things is important for your career as a developer. In most cases, executing on other people’s ideas is going to be what you do for a living. Creating your own side projects is a way for you to explore new concepts, new technologies, and new techniques. Side projects help you grow as a developer in ways that are not yet important to your day job. 

Creating new products, whether you fully execute on the idea and release the product or not, is the same as doing a Code Kata. You do it largely to explore, to innovate, and understand.  If you get far enough along for it to be valuable to others, that’s a bonus. The process of creating is valuable to you. That is why the “Create Something” Life Badge is important. 
<div class="pagebreak" /></div>