# Greatness #
![](/data/manuscript/images/greatness.png)\



When I started writing this talk, I knew that putting this Life Badge on my repo would be controversial. Some people will look at me and say “Cal, you’ve already achieved greatness.” Others will dismiss me as arrogant for striving for somethings as shallow as greatness. They will say that it proves everything they thought about me.

To the first group, I say, no, I have not achieved greatness, I’m not even close. The second group I simply ignore. There are always going to be people looking to tear you down. People who think you need to be “taken down a notch.” There’s just no use in giving them any attention.

There are great people in this world. The funny thing is, who you consider great depends largely on your world view. Many people believe Steve Jobs was a great man, but there are those who will point out his flaws. The same goes for Mother Theresa. While many believe her to be a great woman, even she is not without her detractors. As I said, it is largely a function of your world view. 

Regardless of his failings, though, it is hard to deny the impact that Steve Jobs has had on our lives. He changed how we listened to music. He revolutionized the cell phone market. He introduced us to tablet computing. He has changed so much of how we think about computers and computing. One of my favorite quotes from Steve Jobs sums it up well:

"We're here to put a dent in the universe." -- Steve Jobs

Steve Jobs did not aspire to greatness; he lived it. 

I, however, am not Steve Jobs. I am not great, even though I aspire to greatness. Greatness is a goal. It is a goal just out of my reach, but a goal that I strive for nonetheless.

Many people are much more Jimmy Buffett than Steve jobs, though, I understand that. I am a great fan of the Caribbean bard. 

Jimmy summed up life for so many of us in a line from “The Wino and I know” off his 1974 album “Living and Dying in Three Quarter Time.” In this mostly forgotten song is the line:

“I’m just trying to get by, living quiet and shy, in a world full of pushing and shoving.”

That also happens to be the favorite Jimmy Buffet line of the lovely and talented Kathy. 

A lot of us—I would dare say the vast majority of us—don’t want the Greatness badge on our life’s repo. Most of us just want to get by. That’s fine in the great scheme of things. However, greatness comes in different forms. It is possible to be great without being out front and in the public eye. It is possible to be great in your little corner of the world. We can be great while still being quiet and shy. 

You do not have to strive for greatness to put the Greatness badge on your life’s repo. You just have to be passionate enough about something—anything—to be great at it. 

Most of you will never know my mother. By most standards, she is not great. However, long ago, she earned her Greatness badge. When I was growing up, Mom was a 5th Grade English teacher. At a crucial age, my mother had the opportunity to instill a love of reading into hundreds of children over many years. 

No, it didn’t take on all of them, but even if she only reached a fraction of them, she earned her Greatness badge. She took her talents and determined that she was going to change her little corner of the world, and she did. Mom’s Repo proudly displays her bright green “Greatness” badge.

Look at what you are passionate about. Does it help others? Are you working quietly in your little corner of the world to change things? If so, you are earning your Greatness badge whether you think you are great or not. 

I colored my Greatness badge red. The colorings on all my badges are subjective, and yours will be too. But I am not yet convinced that I’ve done anything that merits the Greatness badge. There are so many more things to do that in the big picture; I still have a lot of work to do. Hopefully, someday I’ll be able to color this one yellow, but today is not that day.
<div class="pagebreak" /></div>